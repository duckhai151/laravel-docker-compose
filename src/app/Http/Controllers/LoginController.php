<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use Auth;
use App\User;


class LoginController extends Controller
{
    public function getLogin() {
    	if (Auth::check()) {
    		return redirect('admin');
    	} else {
    		return view('login');
    	}
    }

    public function postLogin(LoginRequest $request) {
    	$login = [
    		'email' => $request->txtEmail,
    		'password' => $request->txtPassword
    	];
    	if (Auth::attempt($login)) {
    		return redirect('admin');
    	} else {
    		return redirect()->back()->with('status', 'Email hoặc password không hợp lệ !');
    	}
    }

    public function getLogout() {
    	Auth::logout();
    	return redirect('login');
    }
}
