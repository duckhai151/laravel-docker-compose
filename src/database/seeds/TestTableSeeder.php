<?php

use Illuminate\Database\Seeder;

class TestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	['name'=>'admin','email'=>'admin@gmail.com','password'=>Hash::make('123456'),'level'=>'1'],
            ['name'=>'test','email'=>'test@gmail.com','password'=>Hash::make('123456'),'level'=>'0']
        ]);     
    }
}
